-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 10, 2019 at 09:50 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `AdopteUnDevLast`
--

-- --------------------------------------------------------

--
-- Table structure for table `badge`
--

CREATE TABLE IF NOT EXISTS `badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1041 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `badge`
--

INSERT INTO `badge` (`id`, `name`, `img`) VALUES
(1, 'HTML', '1.png'),
(2, 'CSS', '1.png'),
(3, 'JavaScript', '1.png'),
(4, 'PHP', '1.png'),
(5, 'VueJS', '2.png'),
(6, 'ReactJS', '2.png'),
(7, 'AngularJS', '2.png'),
(8, 'Symfony', '6.png'),
(9, 'Laravel', '6.png'),
(10, 'CakePHP', '6.png'),
(11, 'Express', '6.png'),
(12, 'JEE', '6.png'),
(13, 'MySQL', '6.png'),
(14, 'PostgreSQL', '6.png'),
(15, 'NoSQL', '6.png'),
(16, 'OracleSQL', '6.png'),
(17, 'MangoDB', '6.png'),
(18, 'GraphQL', '6.png'),
(19, 'Swift', '5.png'),
(20, 'Objective-C', '5.png'),
(21, 'Kotlin', '5.png'),
(22, 'Java Mobile', '5.png'),
(23, 'ReactNative', '5.png'),
(24, 'Ionic', '5.png'),
(25, 'Cordova', '5.png'),
(26, 'Flutter', '5.png'),
(27, 'Unity', '4.png'),
(28, 'Unreal Engine', '4.png'),
(29, 'Wordpress', '3.png'),
(30, 'Prestashop', '3.png'),
(31, 'Lua', '30.png'),
(32, 'Pascal', '30.png'),
(33, 'GO', '30.png'),
(34, 'Python', '30.png'),
(35, 'Ruby', '30.png'),
(36, 'C', '30.png'),
(37, 'C#', '30.png'),
(38, 'C++', '30.png'),
(39, 'Java', '30.png'),
(40, 'GIT', '30.png'),
(41, 'Azure', '30.png'),
(42, 'Docker', '30.png'),
(43, 'Bash / Shell', '30.png'),
(1001, 'Tir à l\'arc', '7.png'),
(1002, 'Basket', '7.png'),
(1003, 'Baseball', '7.png'),
(1004, 'Football', '7.png'),
(1005, 'Natation', '7.png'),
(1006, 'Trekking / Randonnée', '7.png'),
(1007, 'Golf', '7.png'),
(1008, 'Badminton', '7.png'),
(1009, 'VTT', '7.png'),
(1010, 'Tennis', '7.png'),
(1011, 'Judo', '7.png'),
(1012, 'Tennis de table', '7.png'),
(1013, 'Course', '7.png'),
(1014, 'Athlétisme', '7.png'),
(1015, 'Guitare', '9.png'),
(1016, 'Ukulélé', '9.png'),
(1017, 'Basse', '9.png'),
(1018, 'Batterie', '9.png'),
(1019, 'Piano', '9.png'),
(1020, 'Chant', '9.png'),
(1021, 'MAO', '9.png'),
(1022, 'Kazoo', '9.png'),
(1023, 'Jeux Video', '8.png'),
(1024, 'Jeux de Plateau', '8.png'),
(1025, 'Jeux de Rôle', '8.png'),
(1026, 'Jeux de cartes', '8.png'),
(1027, 'Littérature', '11.png'),
(1028, 'Cinéma', '11.png'),
(1029, 'Théatre', '11.png'),
(1030, 'Sciences', '11.png'),
(1031, 'High-tech', '11.png'),
(1032, 'Peinture', '20.png'),
(1033, 'Sculpture', '20.png'),
(1034, 'Ecriture', '20.png'),
(1035, 'Dessin', '20.png');

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191210094012', '2019-12-10 09:46:15');

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE IF NOT EXISTS `profil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `links` json NOT NULL,
  `pdf_cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_cv` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adopted` tinyint(1) NOT NULL,
  `promo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intro` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E6D6B297A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `user_id`, `phone`, `links`, `pdf_cv`, `link_cv`, `video`, `adopted`, `promo`, `intro`, `city`, `img`, `last_name`, `first_name`, `birthday`) VALUES
(1, 1, '0666219841', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/romain-vr%C3%A9bos-68ba95174/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"https://github.com/RVrebos\", \"gitlab\": \"https://gitlab.com/airbound\"}', '1.pdf', '', 'video.mp4', 1, 'A2 Dev', '', '', '', 'Vrebos', 'Romain', '2000-09-23 00:00:00'),
(2, 2, '0645793588', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/alexis-combes-297042177/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '2.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Combes', 'Alexis', '1994-11-16 00:00:00'),
(3, 3, '0782542767', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/augustin-havas-24a044177/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '', '', 'video.mp4', 1, 'A2 Dev', '', '', '', 'Havas', 'Augustin', '1999-01-03 00:00:00'),
(4, 4, '0622787954', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/benjamin-helou-7650b7176/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"https://github.com/BenjaminHelou\", \"gitlab\": \"https://gitlab.com/BHelou\"}', '4.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Helou', 'Benjamin', '2000-12-03 00:00:00'),
(5, 5, '0673958772', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/nathana%C3%ABl-elisabeth-a4404a177/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '5.pdf', '', 'video.mp4', 1, 'A2 Dev', '', '', '', 'Elisabeth', 'Nathanael', '2000-10-13 00:00:00'),
(6, 6, '0625654143', '{\"fb\": \"https://www.facebook.com/lucas.folliot\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/lucas-folliot-53824a175/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"https://www.gitlab.com/Lucas_F\"}', '6.pdf', '', 'video.mp4', 1, 'A2 Dev', '', '', '', 'Folliot', 'Lucas', '2000-08-02 00:00:00'),
(7, 7, '0761792593', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/jules-masson-6a0778176/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"https://www.gitlab.com/JulesMasson\"}', '7.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Masson', 'Jules', '2000-04-16 00:00:00'),
(8, 8, '0652910191', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/robin-laillier-77b089176/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"https://gitlab.com/RobinLaillier\"}', '8.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Laillier', 'Robin', '1999-05-30 00:00:00'),
(9, 9, '0626810500', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Hardant', 'Martin', '1998-12-25 00:00:00'),
(10, 10, '0760000710', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/hélène-wattiaux-96470b121/\", \"twtr\": \"https://twitter.com/Hellycia_\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '10.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Wattiaux', 'Helene', '1995-10-07 00:00:00'),
(11, 11, '0668350914', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/nils-lecointe-13a134113/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '11.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Lecointe', 'Nils', '1996-01-16 00:00:00'),
(12, 12, '0783391195', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/kevin-combes-077b18175/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '12.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Combes', 'Kevin', '1999-09-19 00:00:00'),
(13, 13, '0649695808', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/louise-louvieaux-a81967175/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '13.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Louvieaux', 'Louise', '1996-06-26 00:00:00'),
(14, 14, '0671124203', '{\"fb\": \"https://www.facebook.com/nicolas.levaufre.9\", \"so\": \"https://stackoverflow.com/users/10811124/nicolas-l\", \"lkdn\": \"https://www.linkedin.com/in/nicolas-levaufre-594363161/\", \"twtr\": \"https://twitter.com/NicolasLevaufre\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"https://github.com/NicolasLevaufre\", \"gitlab\": \"https://gitlab.com/NLevaufre\"}', '14.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Levaufre', 'Nicolas', '1988-12-07 00:00:00'),
(15, 15, '0671120436', '{\"fb\": \"https://www.facebook.com/arthur.foutrel\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/arthur-foutrel-41a779175/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"https://gitlab.com/Aeddan\"}', '15.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Foutrel', 'Arthur', '1996-08-07 00:00:00'),
(16, 16, '0699817784', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://fr.linkedin.com/carl-delacour-1447087a\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '16.pdf', '', 'video.mp4', 1, 'A2 Dev', '', '', '', 'Delacour', 'Carl', '1992-09-24 00:00:00'),
(17, 17, '0668674565', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"https://gitlab.com/Jmeuh\"}', '17.pdf', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Pumont', 'JeanMichel', '1986-01-11 00:00:00'),
(18, 18, '0603889608', '{\"fb\": \"https://www.facebook.com/lucas.labigne\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/lucas-labigne-33617b165/\", \"twtr\": \"https://twitter.com/Lulu_Lab\", \"insta\": \"https://www.instagram.com/lucas__labigne/?hl=fr\", \"other\": \"{}\", \"github\": \"https://github.com/LucasLBGN\", \"gitlab\": \"https://gitlab.com/LucasLBGN\"}', '', '', 'https://www.youtube.com/embed/OqweMDSvAZE', 1, 'A2 Dev', '', '', '', 'Labigne', 'Lucas', '1996-01-23 00:00:00'),
(19, 19, '0626514342', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '', '', 'video.mp4', 0, 'A2 Dev', '', '', '', 'Maxime', 'Marie', '1989-06-02 00:00:00'),
(20, 20, '0606060606', '{\"fb\": \"\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/damien-hebert/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"\", \"gitlab\": \"\"}', '', '', 'video.mp4', 1, 'A2 Dev', '', '', '', '', '', NULL),
(21, 21, '0606060606', '{\"fb\": \"https://www.facebook.com/alexandre.collin.18?ref=bookmarks\", \"so\": \"\", \"lkdn\": \"https://www.linkedin.com/in/alexandre-collin-3b7394118/\", \"twtr\": \"\", \"insta\": \"\", \"other\": \"{}\", \"github\": \"https://github.com/Ithunderbird\", \"gitlab\": \"https://gitlab.com/Ithunderbird\"}', '21.pdf', '', 'video.mp4', 1, 'A2 Dev', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profil_badge`
--

CREATE TABLE IF NOT EXISTS `profil_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id_id` int(11) NOT NULL,
  `profil_id_id` int(11) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B0FC93591B8B387B` (`badge_id_id`),
  KEY `IDX_B0FC935931484513` (`profil_id_id`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profil_badge`
--

INSERT INTO `profil_badge` (`id`, `badge_id_id`, `profil_id_id`, `enable`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1),
(5, 5, 1, 1),
(6, 9, 1, 1),
(7, 13, 1, 1),
(8, 21, 1, 1),
(9, 22, 1, 1),
(10, 29, 1, 1),
(11, 34, 1, 1),
(12, 40, 1, 1),
(13, 42, 1, 1),
(14, 43, 1, 1),
(15, 1023, 1, 1),
(16, 1030, 1, 1),
(17, 1031, 1, 1),
(18, 1, 2, 1),
(19, 2, 2, 1),
(20, 3, 2, 1),
(21, 4, 2, 0),
(22, 5, 2, 0),
(23, 13, 2, 1),
(24, 30, 2, 1),
(25, 34, 2, 1),
(26, 40, 2, 1),
(27, 43, 2, 1),
(28, 1007, 2, 1),
(29, 1018, 2, 1),
(30, 1028, 2, 1),
(31, 1030, 2, 1),
(32, 1031, 2, 1),
(33, 1, 3, 1),
(34, 2, 3, 1),
(35, 3, 3, 1),
(36, 5, 3, 0),
(37, 8, 3, 1),
(38, 13, 3, 1),
(39, 40, 3, 1),
(40, 42, 3, 1),
(41, 43, 3, 1),
(42, 1003, 3, 1),
(43, 1005, 3, 1),
(44, 1009, 3, 1),
(45, 1012, 3, 1),
(46, 1023, 3, 1),
(47, 1024, 3, 1),
(48, 1028, 3, 1),
(49, 1031, 3, 1),
(50, 1, 4, 1),
(51, 2, 4, 1),
(52, 3, 4, 1),
(53, 4, 4, 1),
(54, 5, 4, 1),
(55, 6, 4, 0),
(56, 7, 4, 0),
(57, 8, 4, 0),
(58, 9, 4, 0),
(59, 21, 4, 0),
(60, 23, 4, 1),
(61, 24, 4, 0),
(62, 29, 4, 1),
(63, 34, 4, 1),
(64, 37, 4, 0),
(65, 1023, 4, 1),
(66, 1026, 4, 1),
(67, 1028, 4, 1),
(68, 1, 5, 1),
(69, 2, 5, 1),
(70, 3, 5, 1),
(71, 4, 5, 1),
(72, 7, 5, 1),
(73, 8, 5, 1),
(74, 13, 5, 1),
(75, 24, 5, 1),
(76, 27, 5, 1),
(77, 31, 5, 1),
(78, 34, 5, 1),
(79, 37, 5, 1),
(80, 1025, 5, 1),
(81, 1026, 5, 1),
(82, 1028, 5, 1),
(83, 1, 6, 1),
(84, 2, 6, 1),
(85, 3, 6, 1),
(86, 4, 6, 1),
(87, 5, 6, 1),
(88, 11, 6, 1),
(89, 13, 6, 1),
(90, 17, 6, 1),
(91, 1002, 6, 1),
(92, 1008, 6, 1),
(93, 1012, 6, 1),
(94, 1023, 6, 1),
(95, 1027, 6, 1),
(96, 1, 7, 1),
(97, 2, 7, 1),
(98, 3, 7, 1),
(99, 4, 7, 1),
(100, 5, 7, 1),
(101, 13, 7, 1),
(102, 17, 7, 0),
(103, 22, 7, 1),
(104, 26, 7, 0),
(105, 39, 7, 1),
(106, 40, 7, 0),
(107, 1002, 7, 1),
(108, 1008, 7, 1),
(109, 1023, 7, 1),
(110, 1027, 7, 1),
(111, 1031, 7, 1),
(112, 1035, 7, 1),
(113, 1, 8, 1),
(114, 2, 8, 1),
(115, 3, 8, 1),
(116, 4, 8, 1),
(117, 5, 8, 1),
(118, 13, 8, 1),
(119, 26, 8, 0),
(120, 29, 8, 1),
(121, 34, 8, 1),
(122, 36, 8, 1),
(123, 38, 8, 0),
(124, 43, 8, 1),
(125, 1001, 8, 1),
(126, 1002, 8, 1),
(127, 1005, 8, 1),
(128, 1006, 8, 1),
(129, 1008, 8, 1),
(130, 1012, 8, 1),
(131, 1015, 8, 1),
(132, 1016, 8, 1),
(133, 1017, 8, 1),
(134, 1018, 8, 1),
(135, 1019, 8, 1),
(136, 1023, 8, 1),
(137, 1024, 8, 1),
(138, 1025, 8, 1),
(139, 1028, 8, 1),
(140, 1030, 8, 1),
(141, 1031, 8, 1),
(142, 1035, 8, 1),
(143, 1, 9, 1),
(144, 2, 9, 1),
(145, 3, 9, 0),
(146, 5, 9, 0),
(147, 13, 9, 1),
(148, 28, 9, 1),
(149, 35, 9, 0),
(150, 36, 9, 1),
(151, 38, 9, 1),
(152, 1008, 9, 1),
(153, 1015, 9, 1),
(154, 1020, 9, 1),
(155, 1021, 9, 1),
(156, 1023, 9, 1),
(157, 1024, 9, 1),
(158, 1025, 9, 1),
(159, 1026, 9, 1),
(160, 1028, 9, 1),
(161, 1031, 9, 1),
(162, 1035, 9, 1),
(163, 1, 10, 1),
(164, 2, 10, 1),
(165, 3, 10, 0),
(166, 8, 10, 0),
(167, 27, 10, 0),
(168, 29, 10, 0),
(169, 34, 10, 0),
(170, 35, 10, 0),
(171, 38, 10, 1),
(172, 1008, 10, 1),
(173, 1011, 10, 1),
(174, 1013, 10, 1),
(175, 1018, 10, 1),
(176, 1023, 10, 1),
(177, 1024, 10, 1),
(178, 1028, 10, 1),
(179, 1031, 10, 1),
(180, 1, 11, 1),
(181, 2, 11, 1),
(182, 3, 11, 1),
(183, 4, 11, 0),
(184, 13, 11, 1),
(185, 34, 11, 1),
(186, 36, 11, 1),
(187, 39, 11, 1),
(188, 43, 11, 1),
(189, 1008, 11, 1),
(190, 1020, 11, 1),
(191, 1023, 11, 1),
(192, 1024, 11, 1),
(193, 1026, 11, 1),
(194, 1030, 11, 1),
(195, 1031, 11, 1),
(196, 1, 12, 1),
(197, 2, 12, 1),
(198, 3, 12, 1),
(199, 4, 12, 1),
(200, 5, 12, 1),
(201, 8, 12, 0),
(202, 13, 12, 1),
(203, 29, 12, 1),
(204, 34, 12, 1),
(205, 40, 12, 1),
(206, 1011, 12, 1),
(207, 1012, 12, 1),
(208, 1015, 12, 1),
(209, 1017, 12, 1),
(210, 1018, 12, 1),
(211, 1019, 12, 1),
(212, 1025, 12, 1),
(213, 1028, 12, 1),
(214, 1, 13, 1),
(215, 2, 13, 1),
(216, 3, 13, 0),
(217, 29, 13, 1),
(218, 30, 13, 0),
(219, 1005, 13, 1),
(220, 1034, 13, 1),
(221, 1, 14, 1),
(222, 2, 14, 1),
(223, 3, 14, 1),
(224, 4, 14, 1),
(225, 8, 14, 1),
(226, 13, 14, 1),
(227, 22, 14, 0),
(228, 27, 14, 1),
(229, 29, 14, 1),
(230, 34, 14, 1),
(231, 35, 14, 0),
(232, 36, 14, 1),
(233, 37, 14, 1),
(234, 39, 14, 1),
(235, 40, 14, 1),
(236, 42, 14, 0),
(237, 43, 14, 1),
(238, 1006, 14, 1),
(239, 1009, 14, 1),
(240, 1013, 14, 1),
(241, 1023, 14, 1),
(242, 1, 15, 1),
(243, 2, 15, 1),
(244, 3, 15, 1),
(245, 4, 15, 1),
(246, 8, 15, 1),
(247, 13, 15, 1),
(248, 27, 15, 1),
(249, 29, 15, 1),
(250, 30, 15, 1),
(251, 32, 15, 1),
(252, 34, 15, 1),
(253, 37, 15, 1),
(254, 38, 15, 0),
(255, 39, 15, 0),
(256, 43, 15, 1),
(257, 1023, 15, 1),
(258, 1028, 15, 1),
(259, 1031, 15, 1),
(260, 1035, 15, 1),
(261, 1, 16, 1),
(262, 2, 16, 1),
(263, 3, 16, 1),
(264, 4, 16, 1),
(265, 5, 16, 0),
(266, 8, 16, 0),
(267, 13, 16, 0),
(268, 19, 16, 1),
(269, 21, 16, 0),
(270, 22, 16, 0),
(271, 27, 16, 0),
(272, 34, 16, 0),
(273, 35, 16, 0),
(274, 39, 16, 0),
(275, 40, 16, 0),
(276, 1003, 16, 1),
(277, 1016, 16, 1),
(278, 1023, 16, 1),
(279, 1024, 16, 1),
(280, 1025, 16, 1),
(281, 1026, 16, 1),
(282, 1027, 16, 1),
(283, 1028, 16, 1),
(284, 1034, 16, 1),
(285, 1, 17, 0),
(286, 2, 17, 0),
(287, 3, 17, 0),
(288, 4, 17, 0),
(289, 5, 17, 0),
(290, 27, 17, 0),
(291, 28, 17, 0),
(292, 29, 17, 0),
(293, 34, 17, 1),
(294, 35, 17, 0),
(295, 39, 17, 1),
(296, 40, 17, 0),
(297, 1, 18, 1),
(298, 2, 18, 1),
(299, 3, 18, 1),
(300, 4, 18, 1),
(301, 5, 18, 1),
(302, 7, 18, 1),
(303, 8, 18, 1),
(304, 13, 18, 1),
(305, 29, 18, 1),
(306, 30, 18, 1),
(307, 34, 18, 1),
(308, 36, 18, 1),
(309, 38, 18, 1),
(310, 39, 18, 1),
(311, 40, 18, 1),
(312, 1004, 18, 1),
(313, 1007, 18, 1),
(314, 1008, 18, 1),
(315, 1023, 18, 1),
(316, 1028, 18, 1),
(317, 1030, 18, 1),
(318, 1031, 18, 1),
(319, 1, 19, 0),
(320, 2, 19, 0),
(321, 3, 19, 0),
(322, 4, 19, 0),
(323, 5, 19, 0),
(324, 13, 19, 1),
(325, 22, 19, 0),
(326, 33, 19, 0),
(327, 35, 19, 0),
(328, 39, 19, 1),
(329, 40, 19, 0),
(330, 1018, 19, 1),
(331, 1021, 19, 1),
(332, 1023, 19, 1),
(333, 1028, 19, 1),
(334, 1030, 19, 1),
(335, 1031, 19, 1),
(336, 1, 20, 1),
(337, 2, 20, 1),
(338, 3, 20, 1),
(339, 4, 20, 1),
(340, 5, 20, 1),
(341, 8, 20, 1),
(342, 11, 20, 0),
(343, 13, 20, 1),
(344, 23, 20, 0),
(345, 29, 20, 1),
(346, 1001, 20, 1),
(347, 1009, 20, 1),
(348, 1023, 20, 1),
(349, 1030, 20, 1),
(350, 1031, 20, 1),
(351, 1, 21, 1),
(352, 2, 21, 1),
(353, 3, 21, 1),
(354, 4, 21, 1),
(355, 5, 21, 1),
(356, 8, 21, 1),
(357, 14, 21, 0),
(358, 13, 21, 1),
(359, 19, 21, 0),
(360, 29, 21, 1),
(361, 40, 21, 1),
(362, 42, 21, 0),
(363, 1002, 21, 1),
(364, 1018, 21, 1),
(365, 1028, 21, 1),
(366, 1031, 21, 1);



-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profil_id` int(11) NOT NULL,
  `posted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `validated` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C275ED078` (`profil_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `profil_id`, `posted_by`, `text`, `validated`, `created_at`) VALUES
(1, 1, 'vrebosromain1@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 0, '2019-10-07 00:00:00'),
(2, 1, 'vrebosromain1@gmail.com', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.', 0, '2019-10-07 00:00:00'),
(3, 5, 'vrebosromain1@gmail.com', 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', 0, '2019-10-07 00:00:00'),
(4, 7, 'elisabeth.nathanael@gmail.com', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.', 0, '2019-10-07 00:00:00'),
(5, 18, 'elisabeth.nathanael@gmail.com', 'Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.', 0, '2019-10-07 00:00:00'),
(6, 1, 'benjamin.helou0@gmail.com', 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 0, '2019-10-07 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profil_id_id` int(11) NOT NULL,
  `snapshot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2FB3D0EE31484513` (`profil_id_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `profil_id_id`, `snapshot`, `link`, `name`, `description`) VALUES
(1, 1, '', 'http://vrebos.fr', 'Mon site web (CV)', 'Mon CV web'),
(2, 2, 'mesbijouxandco.png', 'mesbijouxandco.com', 'Réalisation d\'un site PrestaShop', 'Dans le cadre de mon stage j\'ai fait un site de vente de bijoux en ligne pour la societé Agnès Creations'),
(3, 2, 'ferme_de_riou.png', 'http://fermederiou.com/', 'Réalisation d\'un site', 'Pendant ma première année, nous avons avec trois camarades fait un site vitrine pour des particuliers qui gèrent des chambres d\'hôtes'),
(4, 2, 'scrapper.png', '', 'Création d\'un scrapper.png', 'J\'ai ecrit en JavaScript un scrapper permettant de recupérer l\'integralité du catalogue d\'un site en ligne à la demande de ma maître de stage'),
(5, 5, 'HarryPotter.png', '', 'HarryPotter', 'Tableaux des scores des maisons HarryPotter'),
(6, 7, 'resmand.jpg', '', 'Resmand', 'Pense-bête pour commande en bar applicatif'),
(7, 8, 'La_ferme_de_Riou.png', 'http://fermederiou.com/', 'La ferme de Riou', 'C\'est un site que j\'ai fais en fin d\'annee pour un client'),
(8, 12, 'cabinet.png', 'http://cabinetveterinairedelavallee.fr', 'Cabinet de la vallée', 'Site web pour un vétérinaire'),
(9, 14, 'VRV_Controller.png', '', 'VRV Controller', 'Prototype d\'application permettant le pilotage de lecture video 360 dans des casques de realite virtuelle autonomes depuis une telecommande type tablette sous Android, made with Unity'),
(10, 14, '', '', 'HoverBoard', 'Prototype d\'application permettant a son utilisateur de se deplacer dans un environnement 3D sous Occulus Rift en hover-board, utilisant une Wii balance comme manette. Utilisation de l\'API Wii Walker, made with Unity'),
(11, 14, '', '', 'De nombreux autres projets crees sous Unity, en 2D, 3D, AR, VR [a ajouter ulterieurement]', ''),
(12, 15, 'Flubber.png', 'https://youtu.be/7Ta-zZuudwI', 'Projet Flubber', 'Conception d\'un jeux vidéo sous Unity'),
(13, 16, 'Flubber.png', 'https://youtu.be/7Ta-zZuudwI', 'Projet Flubber', 'Vidéo de présentation d\'un projet Unity. Le projet Flubber est un jeux vidéo créé dans le cadre d\'une formation. Concepteurs: Carl Delacour, Nicolas Levaufre, Alexandre Collin et Arthur Foutrel.'),
(14, 20, 'Ganapati.png', 'ganapati.fr', 'Ganapati', 'Service dédié à la formation présentielle et à distance, offre un service gratuit pour construire et piloter des plans de formation.'),
(15, 20, '', '', 'Site des archers Harcourtois', 'Site vitrine du club de tir à l\'arc de Thurry Harcourt');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `roles` json NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `enabled`, `password`, `last_login`, `roles`, `created_at`) VALUES
(1, 'vrebosromain1@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(2, 'combes.alexis.mail@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(3, 'ahavas412@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(4, 'benjamin.helou0@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(5, 'elisabeth.nathanael@gmail.com', 1, '$argon2id$v=19$m=65536,t=4,p=1$PKfzrdDuhpykvYofTDgRkA$LBKw74lQBT7Z8RIO+MplXhxRD2ddhczo26IHSKP5hXw', NULL, '[]', '2019-10-07 00:00:00'),
(6, 'lucas.folliot@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(7, 'jules.masson2000@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(8, 'robin.laillier@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(9, 'martin.hardant@sfr.fr', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(10, 'wattiaux.helene@hotmail.fr', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(11, 'lecointenils@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(12, 'kevin.combes@hotmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(13, 'louiselouvieaux@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(14, 'nicolas.levaufre@hotmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(15, 'arthur.foutrel@laposte.net', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(16, 'delacour.carl@hotmail.fr', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(17, 'jeanmichelpumont@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(18, 'lucas.labigne@gmail.com', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(19, 'marie.maxime14@laposte.net', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(20, '', 1, '', NULL, '[]', '2019-10-07 00:00:00'),
(21, 'mail', 1, '', NULL, '[]', '2019-10-07 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C275ED078` FOREIGN KEY (`profil_id`) REFERENCES `profil` (`id`);

--
-- Constraints for table `profil`
--
ALTER TABLE `profil`
  ADD CONSTRAINT `FK_E6D6B297A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `profil_badge`
--
ALTER TABLE `profil_badge`
  ADD CONSTRAINT `FK_B0FC93591B8B387B` FOREIGN KEY (`badge_id_id`) REFERENCES `badge` (`id`),
  ADD CONSTRAINT `FK_B0FC935931484513` FOREIGN KEY (`profil_id_id`) REFERENCES `profil` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `FK_2FB3D0EE31484513` FOREIGN KEY (`profil_id_id`) REFERENCES `profil` (`id`);
