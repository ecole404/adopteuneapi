<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191210095219 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE badge (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, profil_id INT NOT NULL, posted_by VARCHAR(255) NOT NULL, text LONGTEXT NOT NULL, validated TINYINT(1) DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_9474526C275ED078 (profil_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profil (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, birthday DATETIME NOT NULL, phone VARCHAR(255) NOT NULL, links JSON NOT NULL, pdf_cv VARCHAR(255) NOT NULL, link_cv VARCHAR(255) NOT NULL, video VARCHAR(255) NOT NULL, adopted TINYINT(1) NOT NULL, promo VARCHAR(255) NOT NULL, intro LONGTEXT NOT NULL, city VARCHAR(255) NOT NULL, img VARCHAR(255) NOT NULL, INDEX IDX_E6D6B297A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profil_badge (id INT AUTO_INCREMENT NOT NULL, badge_id_id INT NOT NULL, profil_id_id INT NOT NULL, enable TINYINT(1) NOT NULL, INDEX IDX_B0FC93591B8B387B (badge_id_id), INDEX IDX_B0FC935931484513 (profil_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project (id INT AUTO_INCREMENT NOT NULL, profil_id_id INT NOT NULL, snapshot VARCHAR(255) NOT NULL, link VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_2FB3D0EE31484513 (profil_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, enabled TINYINT(1) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, roles JSON NOT NULL, created_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C275ED078 FOREIGN KEY (profil_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE profil ADD CONSTRAINT FK_E6D6B297A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE profil_badge ADD CONSTRAINT FK_B0FC93591B8B387B FOREIGN KEY (badge_id_id) REFERENCES badge (id)');
        $this->addSql('ALTER TABLE profil_badge ADD CONSTRAINT FK_B0FC935931484513 FOREIGN KEY (profil_id_id) REFERENCES profil (id)');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE31484513 FOREIGN KEY (profil_id_id) REFERENCES profil (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE profil_badge DROP FOREIGN KEY FK_B0FC93591B8B387B');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C275ED078');
        $this->addSql('ALTER TABLE profil_badge DROP FOREIGN KEY FK_B0FC935931484513');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE31484513');
        $this->addSql('ALTER TABLE profil DROP FOREIGN KEY FK_E6D6B297A76ED395');
        $this->addSql('DROP TABLE badge');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE profil');
        $this->addSql('DROP TABLE profil_badge');
        $this->addSql('DROP TABLE project');
        $this->addSql('DROP TABLE user');
    }
}
