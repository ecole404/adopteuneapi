<?php

namespace App\Form;

use App\Entity\Badge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *   request="FormBadge",
 *   description="Badge object",
 *   required=true,
 *   @OA\JsonContent(ref="#/components/schemas/FormBadge"),
 * )
 */

/**
 * @OA\Schema(
 *   schema="FormBadge",
 *   title="Badge",
 *   description="Badge de competence"
 * )
 **/
class BadgeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          /**
           * @OA\Property(
           *   property="name",
           *   ref="#/components/schemas/Badge/properties/name"
           * )
           */
            ->add('name')

          /**
           * @OA\Property(
           *   property="img",
           *   ref="#/components/schemas/Badge/properties/img"
           * )
           */
            ->add('img')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Badge::class,
            'csrf_protection' => false
        ]);
    }
}
