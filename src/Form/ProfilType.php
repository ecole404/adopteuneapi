<?php

namespace App\Form;

use App\Entity\Profil;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *   request="FormProfil",
 *   description="Profil object",
 *   required=true,
 *   @OA\JsonContent(ref="#/components/schemas/FormProfil"),
 * )
 */

/**
 * @OA\Schema(
 *   schema="FormProfil",
 *   title="Profil",
 *   description="Profil"
 * )
 **/
class ProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          /**
           * @OA\Property(
           *   property="lastName",
           *   ref="#/components/schemas/Profil/properties/lastName"
           * )
           */
            ->add('lastName')

          /**
           * @OA\Property(
           *   property="firstName",
           *   ref="#/components/schemas/Profil/properties/firstName"
           * )
           */
            ->add('firstName')

          /**
           * @OA\Property(
           *   property="birthday",
           *   ref="#/components/schemas/Profil/properties/birthday"
           * )
           */
            ->add('birthday')

          /**
           * @OA\Property(
           *   property="phone",
           *   ref="#/components/schemas/Profil/properties/phone"
           * )
           */
            ->add('phone')

          /**
           * @OA\Property(
           *   property="links",
           *   ref="#/components/schemas/Profil/properties/links"
           * )
           */
            ->add('links', CollectionType::class)

          /**
           * @OA\Property(
           *   property="pdfCV",
           *   ref="#/components/schemas/Profil/properties/pdfCV"
           * )
           */
            ->add('pdfCV')

          /**
           * @OA\Property(
           *   property="linkCV",
           *   ref="#/components/schemas/Profil/properties/linkCV"
           * )
           */
            ->add('linkCV')

          /**
           * @OA\Property(
           *   property="video",
           *   ref="#/components/schemas/Profil/properties/video"
           * )
           */
            ->add('video')

          /**
           * @OA\Property(
           *   property="adopted",
           *   ref="#/components/schemas/Profil/properties/adopted"
           * )
           */
            ->add('adopted')

          /**
           * @OA\Property(
           *   property="promo",
           *   ref="#/components/schemas/Profil/properties/promo"
           * )
           */
            ->add('promo')

          /**
           * @OA\Property(
           *   property="intro",
           *   ref="#/components/schemas/Profil/properties/intro"
           * )
           */
            ->add('intro')

          /**
           * @OA\Property(
           *   property="city",
           *   ref="#/components/schemas/Profil/properties/city"
           * )
           */
            ->add('city')

          /**
           * @OA\Property(
           *   property="img",
           *   ref="#/components/schemas/Profil/properties/img"
           * )
           */
            ->add('img')

          /**
           * @OA\Property(
           *   property="user",
           *   ref="#/components/schemas/Profil/properties/user"
           * )
           */
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Profil::class,
            'csrf_protection' => null
        ]);
    }
}
