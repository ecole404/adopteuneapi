<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *   request="FormComment",
 *   description="Comment object",
 *   required=true,
 *   @OA\JsonContent(ref="#/components/schemas/FormComment"),
 * )
 */

/**
 * @OA\Schema(
 *   schema="FormComment",
 *   title="Comment",
 *   description="Commentaire"
 * )
 **/
class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          /**
           * @OA\Property(
           *   property="postedBy",
           *   ref="#/components/schemas/Comment/properties/postedBy"
           * )
           */
            ->add('postedBy')

          /**
           * @OA\Property(
           *   property="text",
           *   ref="#/components/schemas/Comment/properties/text"
           * )
           */
            ->add('text')

          /**
           * @OA\Property(
           *   property="validated",
           *   ref="#/components/schemas/Comment/properties/validated"
           * )
           */
            ->add('validated')

          /**
           * @OA\Property(
           *   property="profil",
           *   ref="#/components/schemas/Comment/properties/profil"
           * )
           */
            ->add('profil')

          /**
           * @OA\Property(
           *   property="createdAt",
           *   ref="#/components/schemas/Comment/properties/createdAt"
           * )
           */
            ->add('createdAt')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
            'csrf_protection' => false
        ]);
    }
}
