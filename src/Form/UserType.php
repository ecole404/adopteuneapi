<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *   request="FormUser",
 *   description="User object",
 *   required=true,
 *   @OA\JsonContent(ref="#/components/schemas/FormUser"),
 * )
 */

/**
 * @OA\Schema(
 *   schema="FormUser",
 *   title="User",
 *   description="User"
 * )
 **/
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          /**
           * @OA\Property(
           *   property="email",
           *   ref="#/components/schemas/User/properties/plainPassword"
           * )
           */
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir une adresse email'
                    ]),
                    new Email([
                        'message' => "L'adresse email n'est pas valide"
                    ])
                ]
            ])

          /**
           * @OA\Property(
           *   property="plainPassword",
           *   ref="#/components/schemas/User/properties/plainPassword"
           * )
           */
            ->add("plainPassword",null, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Vous devez saisir un mot de passe'
                    ])
                ]
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User',
            'csrf_protection' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}