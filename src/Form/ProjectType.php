<?php

namespace App\Form;

use App\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use OpenApi\Annotations as OA;

/**
 * @OA\RequestBody(
 *   request="FormProject",
 *   description="Project object",
 *   required=true,
 *   @OA\JsonContent(ref="#/components/schemas/FormProject"),
 * )
 */

/**
 * @OA\Schema(
 *   schema="FormProject",
 *   title="Project",
 *   description="Project"
 * )
 **/
class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

          /**
           * @OA\Property(
           *   property="snapshot",
           *   ref="#/components/schemas/Project/properties/snapshot"
           * )
           */
            ->add('snapshot')

          /**
           * @OA\Property(
           *   property="link",
           *   ref="#/components/schemas/Project/properties/link"
           * )
           */
            ->add('link')

          /**
           * @OA\Property(
           *   property="name",
           *   ref="#/components/schemas/Project/properties/name"
           * )
           */
            ->add('name')

          /**
           * @OA\Property(
           *   property="description",
           *   ref="#/components/schemas/Project/properties/description"
           * )
           */
            ->add('description')

          /**
           * @OA\Property(
           *   property="profilId",
           *   ref="#/components/schemas/Project/properties/profilId"
           * )
           */
            ->add('profilId')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'csrf_protection' => false
        ]);
    }
}
