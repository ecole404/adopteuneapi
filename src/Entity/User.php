<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="User",
 *   description="Compte priver d'un utilisateur"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64"
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @Groups({"profils","users"})
     */
    private $id;

    /**
     * @OA\Property(ref="#/components/schemas/Profil")
     *
     * @ORM\OneToMany(targetEntity="Profil", mappedBy="user", orphanRemoval=true)
     */
    private $profil;

    /**
     * @OA\Property(
     *   title="email",
     *   type="string",
     *   description="email de connexion"
     * )
     *
     * @ORM\Column(type="string", length=180, unique=true)
     * 
     * @Groups({"profils","users"})
     */
    private $email;

    /**
     * @OA\Property(
     *   title="enabled",
     *   type="boolean",
     *   description="Le compte est-il activé"
     * )
     *
     * @ORM\Column(type="boolean", nullable=true)
     * 
     * @Groups({"users"})
     */
    private $enabled;

    /**
     * @var string The hashed password
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"users"})
     */
    private $password;

    /**
     * @OA\Property(
     *   title="plainPassword",
     *   type="string",
     *   description="Plain pwd"
     * )
     **/
    private $plainPassword;

    /**
     * @OA\Property(
     *   title="lastLogin",
     *   type="string",
     *   format="datetime",
     *   description="Dernier connexion"
     * )
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"users"})
     */
    private $lastLogin;

    /**
     * @ORM\Column(type="json")
     *
     * @Groups({"users"})
     */
    private $roles = [];

    /**
     * @OA\Property(
     *   title="createdAt",
     *   type="string",
     *   format="datetime",
     *   description="Date de créaction du compte"
     * )
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"users"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->enabled = true;
        $this->plainPassword = "";
        $this->profil = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    
    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        //$roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Profil[]
     */
    public function getProfil(): Collection
    {
        return $this->profil;
    }

    public function addProfil(Profil $profil): self
    {
        if (!$this->profil->contains($profil)) {
            $this->profil[] = $profil;
            $profil->setUser($this);
        }

        return $this;
    }

    public function removeProfil(Profil $profil): self
    {
        if ($this->profil->contains($profil)) {
            $this->profil->removeElement($profil);
            // set the owning side to null (unless already changed)
            if ($profil->getUser() === $this) {
                $profil->setUser(null);
            }
        }

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }


    public function createJson()
    {
        return array(
            "id" => $this->id,
            "profil" => $this->profil,
            "email" => $this->email,
            "enabled" => $this->enabled,
            "password" => $this->password,
            "lastLogin" => $this->lastLogin,
            "roles" => $this->roles,
            "createdAt" => $this->createdAt);
    }

}
