<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="ProfilBadge",
 *   description="Lien entre un badge et un profil"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProfilBadgeRepository")
 */
class ProfilBadge
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64",
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @Groups({"profils"})
     */
    private $id;

    /**
     * @OA\Property(ref="#/components/schemas/Badge")
     *
     * @ORM\ManyToOne(targetEntity="Badge", inversedBy="profilBadges")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Groups({"profils"})
     */
    private $badgeId;

    /**
     * @OA\Property(ref="#/components/schemas/Profil")
     *
     * @ORM\ManyToOne(targetEntity="Profil", inversedBy="profilBadges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profilId;

    /**
     * @OA\Property(
     *   type="boolean",
     *   description="true si le profil a acquis le badge, false s'il souhaite l'acquerir"
     * )
     *
     * @ORM\Column(type="boolean")
     * 
     * @Groups({"profils"})
     */
    private $enable;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBadgeId(): ?Badge
    {
        return $this->badgeId;
    }

    public function setBadgeId(?Badge $badgeId): self
    {
        $this->badgeId = $badgeId;

        return $this;
    }

    public function getProfilId(): ?Profil
    {
        return $this->profilId;
    }

    public function setProfilId(?Profil $profilId): self
    {
        $this->profilId = $profilId;

        return $this;
    }

    public function getEnable(): ?bool
    {
        return $this->enable;
    }

    public function setEnable(bool $enable): self
    {
        $this->enable = $enable;

        return $this;
    }
}
