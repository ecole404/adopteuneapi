<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="Badge",
 *   description="Badge"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\BadgeRepository")
 */
class Badge
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64",
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"badges","profils"})
     */
    private $id;

    /**
     * @OA\Property(
     *   title="Name",
     *   type="string",
     *   description="Nom du badge"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"badges","profils"})
     *
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @OA\Property(
     *   title="Img",
     *   type="string",
     *   description="lien de l'image du badge"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"badges","profils"})
     *
     * @Assert\NotBlank
     */
    private $img;

    /**
     * @OA\Property(ref="#/components/schemas/ProfilBadge")
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProfilBadge", mappedBy="badgeId", orphanRemoval=true)
     */
    private $profilBadges;


    
    public function __construct()
    {
        $this->profilBadges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|ProfilBadge[]
     */
    public function getProfilBadges(): Collection
    {
        return $this->profilBadges;
    }

    public function addProfilBadge(ProfilBadge $profilBadge): self
    {
        if (!$this->profilBadges->contains($profilBadge)) {
            $this->profilBadges[] = $profilBadge;
            $profilBadge->setBadgeId($this);
        }

        return $this;
    }

    public function removeProfilBadge(ProfilBadge $profilBadge): self
    {
        if ($this->profilBadges->contains($profilBadge)) {
            $this->profilBadges->removeElement($profilBadge);
            // set the owning side to null (unless already changed)
            if ($profilBadge->getBadgeId() === $this) {
                $profilBadge->setBadgeId(null);
            }
        }

        return $this;
    }
    public function serialize(){
        return Array(
            "id" => $this->id,
            "name" => $this->name,
            "img" => $this->img
        );
    }
}
