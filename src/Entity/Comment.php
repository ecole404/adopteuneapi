<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="Comment",
 *   description="Commentaire applicable sur un profil"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 */
class Comment
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64",
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @Groups({"profils","comments"})
     */
    private $id;

    /**
     * @OA\Property(ref="#/components/schemas/Profil")
     *
     * @ORM\ManyToOne(targetEntity="Profil", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * 
     * @Assert\NotBlank
     */
    private $profil;

    /**
     * @OA\Property(
     *   title="postedBy",
     *   type="string",
     *   description="Nom de la personne ayant posté le commentaire"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"profils","comments"})
     * 
     * @Assert\NotBlank
     */
    private $postedBy;

    /**
     * @OA\Property(
     *   title="text",
     *   type="string",
     *   description="Le contenu du commentaire"
     * )
     *
     * @ORM\Column(type="text")
     * 
     * @Groups({"profils","comments"})
     * 
     * @Assert\NotBlank
     */
    private $text;

    /**
     * @OA\Property(
     *   title="validated",
     *   type="boolean",
     *   description="Le commentaire a été validé ou non par un administrateur",
     *   default="false"
     * )
     *
     * @ORM\Column(type="boolean", nullable=true)
     * 
     * @Groups({"profils","comments"})
     * 
     */
    private $validated;

    /**
     * @OA\Property(
     *   title="createdAt",
     *   type="string",
     *   format="datetime",
     *   description="Date ou a été posté le commentaire"
     * )
     *
     * @ORM\Column(type="string")
     * 
     * @Groups({"profils","comments"})
     * 
     * @Assert\NotBlank
     */
    private $createdAt;

    public function __construct()
    {
        //$this->createdAt = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfil(): ?Profil
    {
        return $this->profil;
    }

    public function setProfil(?Profil $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    public function getPostedBy(): ?string
    {
        return $this->postedBy;
    }

    public function setPostedBy(string $postedBy): self
    {
        $this->postedBy = $postedBy;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?string $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'postedBy' => $this->postedBy,
            'text' => $this->text,
            'validated' => $this->validated,
            'createdAt' => $this->createdAt->format('d/m/Y'),
        );
    }
}
