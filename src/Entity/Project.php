<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="Project",
 *   description="Réalisation site web, logiciel..."
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64",
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @Groups({"profils","projects"})
     */
    private $id;

    /**
     * @OA\Property(ref="#/components/schemas/Profil")
     *
     * @ORM\ManyToOne(targetEntity="Profil", inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Assert\NotBlank
     */
    private $profilId;

    /**
     * @OA\Property(
     *   title="snapshot",
     *   type="string",
     *   description="Description du projet"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils","projects"})
     */
    private $snapshot;

    /**
     * @OA\Property(
     *   title="link",
     *   type="string",
     *   description="Lien externe du projet"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils","projects"})
     */
    private $link;

    /**
     * @OA\Property(
     *   title="name",
     *   type="string",
     *   description="Nom du projet"
     * )
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotNull
     * 
     * @Groups({"profils","projects"})
     */
    private $name;

    /**
     * @OA\Property(
     *   title="description",
     *   type="string",
     *   description="Description du projet"
     * )
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotNull
     * 
     * @Groups({"profils","projects"})
     */
    private $description;

    public function __construct(){
        $this->link = "";
        $this->snapshot = "";
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProfilId(): ?Profil
    {
        return $this->profilId;
    }

    public function setProfilId(?Profil $profilId): self
    {
        $this->profilId = $profilId;

        return $this;
    }

    public function getSnapshot(): ?string
    {
        return $this->snapshot;
    }

    public function setSnapshot(?string $snapshot): self
    {
        $this->snapshot = $snapshot;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->id,
            'snapshot' => $this->snapshot,
            'name' => $this->name,
            'link' => $this->link,
            'description' => $this->description,
        );
    }
}
