<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *   title="Profil",
 *   description="Profil public d'un utilisateur"
 * )
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProfilRepository")
 */
class Profil
{
    /**
     * @OA\Property(
     *     title="ID",
     *     format="int64"
     * )
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * 
     * @Groups({"profils"})
     */
    private $id;

    /**
     * @OA\Property(
     *   title="lastName",
     *   type="string",
     *   description="Nom du profil"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"profils"})
     * 
     * @Assert\NotBlank 
     */
    private $lastName;

    /**
     * @OA\Property(
     *   title="firstName",
     *   type="string",
     *   description="Prenom du profil"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"profils"})
     * 
     * @Assert\NotBlank 
     */
    private $firstName;

    /**
     * @OA\Property(
     *   title="birthday",
     *   type="string",
     *   description="Date de naisance"
     * )
     *
     * @ORM\Column(type="string")
     * 
     * @Groups({"profils"})
     * 
     * @Assert\NotBlank 
     */
    private $birthday;

    /**
     * @OA\Property(
     *   title="phone",
     *   type="string",
     *   description="Telephone"
     * )
     *
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"profils"})
     * 
     * @Assert\NotBlank 
     */
    private $phone;

    /**
     * @OA\Property(
     *   title="links",
     *   type="string",
     *   format="json",
     *   description="Lien des reseaux sociaux"
     * )
     *
     * @ORM\Column(type="json", nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $links = [];

    /**
     * @OA\Property(
     *   title="pdfCV",
     *   type="string",
     *   description="Lien du cv"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $pdfCV;

    /**
     * @OA\Property(
     *   title="linkCV",
     *   type="string",
     *   description="Lien d'un site cv"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $linkCV;

    /**
     * @OA\Property(
     *   title="video",
     *   type="string",
     *   description="Lien d'une video de présentation"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $video;

    /**
     * @OA\Property(
     *   title="adopted",
     *   type="boolean",
     *   description="L'utilisateur à t'il trouvé sont alternance"
     * )
     *
     * @ORM\Column(type="boolean")
     * 
     * @Groups({"profils"})
     * 
     * @Assert\NotBlank 
     */
    private $adopted;

    /**
     * @OA\Property(
     *   title="promo",
     *   type="string",
     *   description="Promotion dans la quelle étudie l'utilisateur",
     *   example="A2 DEV"
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $promo;

    /**
     * @OA\Property(
     *   title="intro",
     *   type="string",
     *   description="Présentation de l'utilisateur",
     * )
     *
     * @ORM\Column(type="text", nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $intro;

    /**
     * @OA\Property(
     *   title="city",
     *   type="string",
     *   description="Ville d'habitation de l'utilisateur",
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $city;

    /**
     * @OA\Property(
     *   title="img",
     *   type="string",
     *   description="Photo de profil de l'utilisateur",
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"profils"})
     */
    private $img;

    /**
     * @OA\Property(ref="#/components/schemas/ProfilBadge")
     *
     * @ORM\OneToMany(targetEntity="ProfilBadge", mappedBy="profilId", orphanRemoval=true)
     * 
     * @Groups({"profils"})
     * 
     * @MaxDepth(1)
     */
    private $profilBadges;

    /**
     * @OA\Property(
     *   title="projects",
     *   type="array",
     *   @OA\Items(ref="#/components/schemas/Project")
     * )
     *
     * @ORM\OneToMany(targetEntity="Project", mappedBy="profilId", orphanRemoval=true)
     * 
     * @Groups({"profils"})
     */
    private $projects;

    /**
     * @OA\Property(ref="#/components/schemas/User")
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="profil")
     * 
     * @Assert\NotBlank 
     */
    private $user;

    /**
     * @OA\Property(
     *   title="comments",
     *   type="array",
     *   @OA\Items(ref="#/components/schemas/Comment")
     * )
     *
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="profil", orphanRemoval=true)
     * 
     * @Groups({"profils"})
     */
    private $comments;

    private $score;

    public function __construct()
    {
        $this->profilBadges = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(?string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getLinks(): ?array
    {
        return $this->links;
    }

    public function setLinks(array $links): self
    {
        $this->links = $links;

        return $this;
    }

    public function getPdfCV(): ?string
    {
        return $this->pdfCV;
    }

    public function setPdfCV(string $pdfCV): self
    {
        $this->pdfCV = $pdfCV;

        return $this;
    }

    public function getLinkCV(): ?string
    {
        return $this->linkCV;
    }

    public function setLinkCV(string $linkCV): self
    {
        $this->linkCV = $linkCV;

        return $this;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    public function setVideo(string $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getAdopted(): ?bool
    {
        return $this->adopted;
    }

    public function setAdopted(bool $adopted): self
    {
        $this->adopted = $adopted;

        return $this;
    }

    public function getPromo(): ?string
    {
        return $this->promo;
    }

    public function setPromo(string $promo): self
    {
        $this->promo = $promo;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(string $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(string $img): self
    {
        $this->img = $img;

        return $this;
    }

    /**
     * @return Collection|ProfilBadge[]
     */
    public function getProfilBadges(): Collection
    {
        return $this->profilBadges;
    }

    public function addProfilBadge(ProfilBadge $profilBadge): self
    {
        if (!$this->profilBadges->contains($profilBadge)) {
            $this->profilBadges[] = $profilBadge;
            $profilBadge->setProfilId($this);
        }

        return $this;
    }

    public function removeProfilBadge(ProfilBadge $profilBadge): self
    {
        if ($this->profilBadges->contains($profilBadge)) {
            $this->profilBadges->removeElement($profilBadge);
            // set the owning side to null (unless already changed)
            if ($profilBadge->getProfilId() === $this) {
                $profilBadge->setProfilId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setProfilId($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getProfilId() === $this) {
                $project->setProfilId(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setProfil($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getProfil() === $this) {
                $comment->setProfil(null);
            }
        }

        return $this;
    }

    public function setScore($score){
        $this->$score = $score;
    }
    public function getScore(){
        return $this->score;
    }


    public function __toString(){
        return $this->firstName;
    }

}
