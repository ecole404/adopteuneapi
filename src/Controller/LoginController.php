<?php

namespace App\Controller;

use App\Controller\BaseController;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;
use App\Repository\UserRepository;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class LoginController extends BaseController
{
    /**
     * @author Elisabeth Nathanaël

     * @Rest\Post("/api/login_check")
     */
    public function LoginAction(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder, JWTTokenManagerInterface $JWTManager)
    {
        if($request->request->has("mail") && $request->request->has("password")){

            $user = $userRepository->findOneBy(["email" => $request->request->get("mail")]);
            $isPasswordValid = $encoder->isPasswordValid($user, $request->request->get("password"));
            
            if($isPasswordValid == true) {

                return new JsonResponse(['token' => $JWTManager->create($user)]);
            }
        }
    }
}