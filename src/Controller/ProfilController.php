<?php

namespace App\Controller;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ProfilRepository;
use App\Entity\Profil;
use App\Form\ProfilType;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;


class ProfilController extends BaseController
{
  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/profils",
   *   tags={"profils"},
   *   @OA\Response(
   *    description="Liste des profils",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/Profil")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/profils")
   *
   * @param ProfilRepository $ProfilRepository
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllProfils(ProfilRepository $ProfilRepository, SerializerInterface $serializer)
    {
        $rawProfils = $ProfilRepository->findAll();
        return new JsonResponse(json_decode($serializer->serialize($rawProfils, 'json', [ 'groups' => ['profils']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/profils/{id}",
   *   tags={"profils"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du profil",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Profil/properties/id")
   *   ),
   *   @OA\Response(
   *    description="Un profil",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Profil")
   *   ),
   *   @OA\Response(
   *    description="Profil introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/profils/{id}")
   *
   * @param Profil $profil
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getOneProfil(Profil $profil, SerializerInterface $serializer){
        return new JsonResponse(json_decode($serializer->serialize($profil, 'json', [ 'groups' => ['profils']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/profils/{id}/image",
   *   tags={"profils", "images"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du profil",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Profil/properties/id")
   *   ),
   *   @OA\Response(
   *    description="L'image du profil",
   *    response="200",
   *    @OA\MediaType(
   *      mediaType="image/*"
   *    )
   *   ),
   *   @OA\Response(
   *    description="Profil ou Image introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/profils/{id}/image")
   *
   * @param Profil $profil
   * @param ParameterBagInterface $parameterInterface
   *
   * @return BinaryFileResponse
   */
    public function profilImageAction(Profil $profil, ParameterBagInterface $parameterInterface) {
        $idImage = $profil->getId();
        $path = $parameterInterface->get('kernel.project_dir').'/public/asset/profils/pictures/'.$idImage.'.png';
        return new BinaryFileResponse($path);
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Post(
   *   path="/api/v2/admin/profils",
   *   tags={"admin", "profils"},
   *   @OA\RequestBody(ref="#/components/requestBodies/FormProfil"),
   *   @OA\Response(
   *    description="Le profil créé",
   *    response="201",
   *    @OA\JsonContent(ref="#/components/schemas/Profil")
   *   ),
   *   @OA\Response(
   *    description="Donnée du profil fourni invalide",
   *    response="400"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Post("/api/v2/admin/profils")
   *
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function createOneProfil(Request $request, EntityManagerInterface $em, SerializerInterface $serializer){
        $profil = new Profil();

        $form = $this->createForm(ProfilType::class, $profil);
        $form->submit($request->request->all());

        if($form->isValid()){
            
            //$profil->setBirthday($form['birthday']->getData());

            $em->persist($profil);
            $em->flush();

            return new JsonResponse(json_decode($serializer->serialize($profil, 'json', [ 'groups' => ['profils']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Put(
   *   path="/api/v2/admin/profils/{id}",
   *   tags={"admin", "profils"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du profil",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Profil/properties/id")
   *   ),
   *   @OA\RequestBody(ref="#/components/requestBodies/FormProfil"),
   *   @OA\Response(
   *    description="Le profil mis à jour",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Profil")
   *   ),
   *   @OA\Response(
   *    description="Donnée du profil fourni invalide",
   *    response="400"
   *   ),
   *   @OA\Response(
   *    description="Profil introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Put("/api/v2/admin/profils/{id}")
   *
   * @param Profil $profil
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function editProfil(Profil $profil, Request $request, EntityManagerInterface $em, SerializerInterface $serializer){
 
        $form = $this->createForm(ProfilType::class, $profil);
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($profil);
            $em->flush();

            return new JsonResponse(json_decode($serializer->serialize($profil, 'json', [ 'groups' => ['profils']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Delete(
   *   path="/api/v2/admin/profils/{id}",
   *   tags={"admin", "profils"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du profil",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Profil/properties/id")
   *   ),
   *   @OA\Response(
   *    description="OK",
   *    response="204"
   *   ),
   *   @OA\Response(
   *    description="Profil introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
   * @Rest\Delete("/api/v2/admin/profils/{id}")
   *
   * @param Profil $profil
   * @param EntityManagerInterface $em
   */
    public function deleteProfil(Profil $profil, EntityManagerInterface $em){
        $em->remove($profil);
        $em->flush();
    }
}
