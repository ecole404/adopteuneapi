<?php

namespace App\Controller;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Project;
use App\Form\ProjectType;
use App\Repository\ProjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;

class ProjectController extends BaseController
{
  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/projects",
   *   tags={"projects"},
   *   @OA\Response(
   *    description="Liste des projets",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/Project")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/projects")
   *
   * @param ProjectRepository $projectRepositorys
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllProjects(ProjectRepository $projectRepositorys, SerializerInterface $serializer)
    {
        $rawProjects = $projectRepositorys->findAll();
        return new JsonResponse(json_decode($serializer->serialize($rawProjects, 'json', [ 'groups' => ['projects']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/projects/{id}",
   *   tags={"projects"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du projet",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Project/properties/id")
   *   ),
   *   @OA\Response(
   *    description="Un projet",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Project")
   *   ),
   *   @OA\Response(
   *    description="Projet introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/projects/{id}")
   *
   * @param Project $project
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function GetOneProjects(Project $project, SerializerInterface $serializer)
    {
        return new JsonResponse(json_decode($serializer->serialize($project, 'json', [ 'groups' => ['projects']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/projects/{id}/image",
   *   tags={"badges", "projects"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du projet",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Project/properties/id")
   *   ),
   *   @OA\Response(
   *    description="L'image du projet",
   *    response="200",
   *    @OA\MediaType(
   *      mediaType="image/*"
   *    )
   *   ),
   *   @OA\Response(
   *    description="Projet ou Image introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/projects/{id}/image")
   *
   * @param Project $project
   * @param ParameterBagInterface $parameterInterface
   *
   * @return BinaryFileResponse|JsonResponse
   */
    public function badgeImageAction(Project $project, ParameterBagInterface $parameterInterface) {
        if($idImage = $project->getSnapshot()){
            $path = $parameterInterface->get('kernel.project_dir').'/public/asset/realisations/'.$idImage;
            return new BinaryFileResponse($path);
        }
        return new JsonResponse("",Response::HTTP_NOT_FOUND);
        
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Post(
   *   path="/api/v2/admin/projects",
   *   tags={"admin", "projects"},
   *   @OA\RequestBody(ref="#/components/requestBodies/FormProject"),
   *   @OA\Response(
   *    description="Le projet créé",
   *    response="201",
   *    @OA\JsonContent(ref="#/components/schemas/Project")
   *   ),
   *   @OA\Response(
   *    description="Donnée du projet fourni invalide",
   *    response="400"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_CREATED)
   * @Rest\Post("/api/v2/admin/projects")
   *
   * @param Request $request
   * @param SerializerInterface $serializer
   * @param EntityManagerInterface $em
   *
   * @return FormInterface|JsonResponse
   */
    public function createProjects(Request $request , SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $project = new Project();

        $form = $this->createForm(ProjectType::class,$project);
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($project);
            $em->flush();
            return new JsonResponse(json_decode($serializer->serialize($project, 'json', [ 'groups' => ['projects']])));

        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Put(
   *   path="/api/v2/admin/projects/{id}",
   *   tags={"admin", "projects"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du projet",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Project/properties/id")
   *   ),
   *   @OA\RequestBody(ref="#/components/requestBodies/FormProject"),
   *   @OA\Response(
   *    description="Le projet mis à jour",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Project")
   *   ),
   *   @OA\Response(
   *    description="Donnée du projet fourni invalide",
   *    response="400"
   *   ),
   *   @OA\Response(
   *    description="Projet introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Put("/api/v2/admin/projects/{id}")
   *
   * @param Project $project
   * @param Request $request
   * @param SerializerInterface $serializer
   * @param EntityManagerInterface $em
   *
   * @return FormInterface|JsonResponse
   */
    public function updateProjects(Project $project, Request $request , SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $form = $this->createForm(ProjectType::class,$project);
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($project);
            $em->flush();
            return new JsonResponse(json_decode($serializer->serialize($project, 'json', [ 'groups' => ['projects']])));

        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Delete(
   *   path="/api/v2/admin/projects/{id}",
   *   tags={"admin", "projects"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du projet",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Project/properties/id")
   *   ),
   *   @OA\Response(
   *    description="OK",
   *    response="204"
   *   ),
   *   @OA\Response(
   *    description="Projet introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
   * @Rest\Delete("/api/v2/admin/projects/{id}")
   *
   * @param Project $project
   * @param EntityManagerInterface $em
   */
    public function deleteProjects(Project $project, EntityManagerInterface $em)
    {
        $em->remove($project);
        $em->flush();
    }

}
