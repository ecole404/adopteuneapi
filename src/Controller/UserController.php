<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;

class UserController extends Controller
{

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/users",
   *   tags={"users"},
   *   @OA\Response(
   *    description="Liste des utilisateurs",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/User")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/users")
   *
   * @param UserRepository $userRepository
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllUsers(UserRepository $userRepository, SerializerInterface $serializer){
        $rawUsers = $userRepository->findAll();
        return new JsonResponse(json_decode($serializer->serialize($rawUsers, 'json', [ 'groups' => ['users']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/users/{id}",
   *   tags={"users"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id de l'utilisateur",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/User/properties/id")
   *   ),
   *   @OA\Response(
   *    description="Un utilisateur",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/User")
   *   ),
   *   @OA\Response(
   *    description="Utilisateur introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/users/{id}")
   *
   * @param User $user
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getOneUser(User $user, SerializerInterface $serializer){
        return new JsonResponse(json_decode($serializer->serialize($user, 'json', [ 'groups' => ['users']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Post(
   *   path="/api/v2/admin/users",
   *   tags={"admin", "users"},
   *   @OA\RequestBody(ref="#/components/requestBodies/FormUser"),
   *   @OA\Response(
   *    description="L'utilisateur créé",
   *    response="201",
   *    @OA\JsonContent(ref="#/components/schemas/User")
   *   ),
   *   @OA\Response(
   *    description="Donnée de l'utilisateur fourni invalide",
   *    response="400"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_CREATED)
   * @Rest\Post("/api/v2/admin/users")
   *
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param UserPasswordEncoderInterface $encoder
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function createOneUser(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, SerializerInterface $serializer){
        $user = new User();
        
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());

        if($form->isValid()){

            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);

            $em->persist($user);
            $em->flush();

            return new JsonResponse(json_decode($serializer->serialize($user, 'json', [ 'groups' => ['users']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Put(
   *   path="/api/v2/admin/users/{id}",
   *   tags={"admin", "users"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id de l'utilisateur",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/User/properties/id")
   *   ),
   *   @OA\RequestBody(ref="#/components/requestBodies/FormUser"),
   *   @OA\Response(
   *    description="L'utilisateur mis à jour",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/User")
   *   ),
   *   @OA\Response(
   *    description="Donnée de l'utilisateur fourni invalide",
   *    response="400"
   *   ),
   *   @OA\Response(
   *    description="Utilisateur introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Put("/api/v2/admin/users/{id}")
   *
   * @param User $user
   * @param Request $request
   * @param UserPasswordEncoderInterface $encoder
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function editUser(User $user, Request $request, UserPasswordEncoderInterface $encoder, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());
        
        if ($form->isValid()) {

            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);

            $em->persist($user);
            $em->flush();
            
            return new JsonResponse(json_decode($serializer->serialize($user, 'json', [ 'groups' => ['users']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Delete(
   *   path="/api/v2/admin/users/{id}",
   *   tags={"admin", "users"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id de l'utilisateur",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/User/properties/id")
   *   ),
   *   @OA\Response(
   *    description="OK",
   *    response="204"
   *   ),
   *   @OA\Response(
   *    description="Utilisateur introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
   * @Rest\Delete("/api/v2/admin/users/{id}")
   *
   * @param User $user
   * @param EntityManagerInterface $em
   */
    public function deleteUser(User $user, EntityManagerInterface $em)
    {
        $em->remove($user);
        $em->flush();
    }
}
