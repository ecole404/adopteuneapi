<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;

class CommentsController extends BaseController
{
  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/comments",
   *   tags={"comments"},
   *   @OA\Response(
   *    description="Liste des commentaire",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/Comment")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/comments")
   *
   * @param CommentRepository $commentRepository
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllComments(CommentRepository $commentRepository, SerializerInterface $serializer)
    {
        $rawComments = $commentRepository->findAll();
        return new JsonResponse(json_decode($serializer->serialize($rawComments, 'json', [ 'groups' => ['comments']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/comments/validated/{validStatu}",
   *   tags={"comments"},
   *   @OA\Parameter(
   *     name="validStatu",
   *     in="path",
   *     description="Status de la validation du commentaire",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Comment/properties/validated")
   *   ),
   *   @OA\Response(
   *    description="Liste des commentaire validé ou non validé",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/Comment")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/comments/validated/{validStatu}")
   *
   * @param Request $request
   * @param CommentRepository $commentRepository
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllValidComments(Request $request, CommentRepository $commentRepository, SerializerInterface $serializer)
    {
        $rawComments = $commentRepository->findBy(["validated" => $request->get("validStatu")]);
        return new JsonResponse(json_decode($serializer->serialize($rawComments, 'json', [ 'groups' => ['comments']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/comments/{id}",
   *   tags={"comments"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du commentaire",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Comment/properties/id")
   *   ),
   *   @OA\Response(
   *    description="Un Commentaire",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Comment")
   *   ),
   *   @OA\Response(
   *    description="Commentaire introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/comments/{id}")
   *
   * @param Comment $comment
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getComment(Comment $comment, SerializerInterface $serializer)
    {
        return new JsonResponse(json_decode($serializer->serialize($comment, 'json', [ 'groups' => ['comments']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Post(
   *   path="/api/v2/admin/comments",
   *   tags={"admin", "comments"},
   *   @OA\RequestBody(ref="#/components/requestBodies/FormComment"),
   *   @OA\Response(
   *    description="Le commenatire créé",
   *    response="201",
   *    @OA\JsonContent(ref="#/components/schemas/Comment")
   *   ),
   *   @OA\Response(
   *    description="Donnée du commenatire fourni invalide",
   *    response="400"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_CREATED)
   * @Rest\Post("/api/v2/admin/comments")
   *
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function createComment(Request $request, EntityManagerInterface $em, SerializerInterface $serializer) {
        $comment = new Comment();

        $form= $this->createForm(CommentType::class, $comment);
        $form->submit($request->request->all());

        if ($form->isValid()){

            $em->persist($comment);
            $em->flush();
            
            return new JsonResponse(json_decode($serializer->serialize($comment, 'json', [ 'groups' => ['comments']])));
        } else {
            
            return $form;
        }

    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Put(
   *   path="/api/v2/admin/comments/{id}",
   *   tags={"admin", "comments"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du commentaire",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Comment/properties/id")
   *   ),
   *   @OA\RequestBody(ref="#/components/requestBodies/FormComment"),
   *   @OA\Response(
   *    description="Le commentaire mis à jour",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Comment")
   *   ),
   *   @OA\Response(
   *    description="Donnée du commentaire fourni invalide",
   *    response="400"
   *   ),
   *   @OA\Response(
   *    description="Commentaire introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Put("/api/v2/admin/comments/{id}")
   *
   * @param Comment $comment
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function editComment(Comment $comment, Request $request, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $form= $this->createForm(CommentType::class, $comment);
        $form->submit($request->request->all());

        if ($form->isValid()){

            $em->persist($comment);
            $em->flush();
            
            return new JsonResponse(json_decode($serializer->serialize($comment, 'json', [ 'groups' => ['comments']])));
        } else {
            
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Delete(
   *   path="/api/v2/admin/comments/{id}",
   *   tags={"admin", "comments"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du commentaire",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Comment/properties/id")
   *   ),
   *   @OA\Response(
   *    description="OK",
   *    response="204"
   *   ),
   *   @OA\Response(
   *    description="Commentaire introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
   * @Rest\Delete("/api/v2/admin/comments/{id}")
   *
   * @param Comment $comment
   * @param EntityManagerInterface $em
   */
    public function deleteComment(Comment $comment, EntityManagerInterface $em) {
        $em->remove($comment);
        $em->flush();
    }
}
