<?php

namespace App\Controller;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\BadgeRepository;
use App\Form\BadgeType;
use App\Entity\Badge;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;

class BadgeController extends BaseController
{
  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/badges",
   *   tags={"badges"},
   *   @OA\Response(
   *    description="Liste des badges",
   *    response="200",
   *    @OA\JsonContent(
   *      @OA\Items(ref="#/components/schemas/Badge")
   *    )
   *  )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/badges")
   *
   * @param BadgeRepository $badgeRepository
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getAllBadges(BadgeRepository $badgeRepository, SerializerInterface $serializer){
        $rawBadges = $badgeRepository->findAll();
        return new JsonResponse(json_decode($serializer->serialize($rawBadges, 'json', [ 'groups' => ['badges']])));
    }

  /**

   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/badges/{id}",
   *   tags={"badges"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du badge",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Badge/properties/id")
   *   ),
   *   @OA\Response(
   *    description="Un badge",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Badge")
   *   ),
   *   @OA\Response(
   *    description="Badge introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/badges/{id}")
   *
   * @param Badge $badge
   * @param SerializerInterface $serializer
   *
   * @return JsonResponse
   */
    public function getOneBadge(Badge $badge, SerializerInterface $serializer)
    {
        return new JsonResponse(json_decode($serializer->serialize($badge, 'json', [ 'groups' => ['badges']])));
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Get(
   *   path="/api/v2/badges/{id}/image",
   *   tags={"badges", "images"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du badge",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Badge/properties/id")
   *   ),
   *   @OA\Response(
   *    description="L'image du badge",
   *    response="200",
   *    @OA\MediaType(
   *      mediaType="image/*"
   *    )
   *   ),
   *   @OA\Response(
   *    description="Badge ou Image introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Get("/api/v2/badges/{id}/image")
   *
   * @param Badge $badge
   * @param ParameterBagInterface $parameterInterface
   *
   * @return BinaryFileResponse|Response
   */
    public function badgeImageAction(Badge $badge, ParameterBagInterface $parameterInterface) {
        if ($idImage = $badge->getImg()) {
          $path = $parameterInterface->get('kernel.project_dir').'/public/asset/badges/'.$idImage;
          return new BinaryFileResponse($path);
        } else {
          return new Response(null, Response::HTTP_NOT_FOUND);
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Post(
   *   path="/api/v2/admin/badges",
   *   tags={"admin", "badges"},
   *   @OA\RequestBody(ref="#/components/requestBodies/FormBadge"),
   *   @OA\Response(
   *    description="Le badge créé",
   *    response="201",
   *    @OA\JsonContent(ref="#/components/schemas/Badge")
   *   ),
   *   @OA\Response(
   *    description="Donnée du badge fourni invalide",
   *    response="400"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_CREATED)
   * @Rest\Post("/api/v2/admin/badges")
   *
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function createOneBadge(Request $request, EntityManagerInterface $em, SerializerInterface $serializer){
        
        $badge = new Badge();
        
        $form = $this->createForm(BadgeType::class, $badge);
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($badge);
            $em->flush();
            
            return new JsonResponse(json_decode($serializer->serialize($badge, 'json', [ 'groups' => ['badges']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Put(
   *   path="/api/v2/admin/badges/{id}",
   *   tags={"admin", "badges"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du badge",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Badge/properties/id")
   *   ),
   *   @OA\RequestBody(ref="#/components/requestBodies/FormBadge"),
   *   @OA\Response(
   *    description="Le badge mis à jour",
   *    response="200",
   *    @OA\JsonContent(ref="#/components/schemas/Badge")
   *   ),
   *   @OA\Response(
   *    description="Donnée du badge fourni invalide",
   *    response="400"
   *   ),
   *   @OA\Response(
   *    description="Badge introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_OK)
   * @Rest\Put("/api/v2/admin/badges/{id}")
   *
   * @param Badge $badge
   * @param Request $request
   * @param EntityManagerInterface $em
   * @param SerializerInterface $serializer
   *
   * @return FormInterface|JsonResponse
   */
    public function modifyBadge(Badge $badge, Request $request, EntityManagerInterface $em, SerializerInterface $serializer){
        $form = $this->createForm(BadgeType::class, $badge);
        $form->submit($request->request->all());

        if($form->isValid()){

            $em->persist($badge);
            $em->flush();

            return new JsonResponse(json_decode($serializer->serialize($badge, 'json', [ 'groups' => ['badges']])));
        }else{
            return $form;
        }
    }

  /**
   * @author Elisabeth Nathanaël
   *
   * @OA\Delete(
   *   path="/api/v2/admin/badges/{id}",
   *   tags={"admin", "badges"},
   *   @OA\Parameter(
   *     name="id",
   *     in="path",
   *     description="Id du badge",
   *     required=true,
   *     @OA\Schema(ref="#/components/schemas/Badge/properties/id")
   *   ),
   *   @OA\Response(
   *    description="OK",
   *    response="204"
   *   ),
   *   @OA\Response(
   *    description="Badge introuvable",
   *    response="404"
   *   )
   * )
   *
   * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
   * @Rest\Delete("/api/v2/admin/badges/{id}")
   *
   * @param Badge $badge
   * @param EntityManagerInterface $em
   */
    public function deleteBadge(Badge $badge, EntityManagerInterface $em){      
        $em->remove($badge);
        $em->flush();
    }

}
