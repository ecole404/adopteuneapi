SI VOUS AVEZ LA BDD :
    faite cette commande :
        php bin/console make:migration
    SI ELLE FOIRE :
        supprimer votre bdd dans phpMyAdmin
        puis suiver le SI VOUS N'AVEZ PAS LA BDD

SI VOUS N'AVEZ PAS LA BDD :
    faite les commande suivante:
        php bin/console doctrine:database:create
        php bin/console make:migration

    puis dans phpMyAdmin :
        importer le dump.sql

POUR LANCER LE SERVEUR :
    php bin/console server:run

SI VOUS AVEZ DES ERREURS :
    ouvres votre navigatuer et allez sur ce lien :
        http://localhost:8000/api/v2/profils
    
    si l'erreur est une erreur SQL :
        Avez vous lancer MAMP // WAMP   ?
        Avez vous correctement configuré votre .env ?
    
    si ce n'est pas une erreur SQL demander à un résponsable API ne rester pas bloquer
    Et si c'est une erreur SQL et que malgrer tout ça vous ne vous en sortez pas demander à un résponsable

Cordialement
